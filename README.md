# Apprendre à utiliser git 
Dès aujourd'hui, commencez à sauvegarder facilement votre travail et tout l'historique de vos modifications avec cet outil gratuit.

## Exemples issus du cours
Ce dépôt est utilisé dans le cours de [Purple Giraffe](https://www.purplegiraffe.fr) pour présenter et expliquer l'utilisation de git.

# Sommaire
* [Installation](Install.md)
* [Travailler au quotidien](Daily.md)
